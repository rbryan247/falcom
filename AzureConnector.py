import sqlite3

class AzureConnector:
    def CreateAlerts():
            # connecting to the database
            connection = sqlite3.connect("alerts.db")
            
            # cursor
            crsr = connection.cursor()

            
            # SQL command to create a table in the database
            sql_command = """CREATE TABLE Alerts ( 
            alertName VARCHAR(20) PRIMARY KEY, 
            configurationItem VARCHAR(20), 
            priority VARCHAR(10), 
            datasource VARCHAR(10), 
            createdAt DATE
            lastUpdated DATE
            updatedBy VARCHAR FOREIGN KEY);"""
            
            # execute the statement
            crsr.execute(sql_command)

            # SQL command to insert the data in the table
            sql_command = """INSERT INTO Alerts VALUES ("Alerts12345", "Forge Energy Optimization",
            "P1", "SCOM", "1980-10-28", "2014-03-28", "2015-10-26");"""
            crsr.execute(sql_command)
                    
            connection.commit()

            connection.close()

"""    if __name__ == '__main__':
        CreateAlerts()"""